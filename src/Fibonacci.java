

public class Fibonacci {

	private static final int MAX_TERM_VALUE = 10000;

    public static void main(String[] args) {

        int n1 = 0;
        int n2 = 1;
        int n3;
        n3 = n1 + n2;
        System.out.println(n1);
        System.out.println(n2);
       
        while (n3 < MAX_TERM_VALUE) {

            System.out.println(n3);
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;


        }
    }


}