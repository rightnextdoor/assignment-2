import java.awt.Color;
import acm.program.*;
import acm.graphics.*;


public class DrawingFace extends GraphicsProgram {
	
	private static final double EYE_WIDTH = 0.15;
	private static final double EYE_HEIGHT = 0.15;
	private static final double MOUTH_WIDTH  = 0.50;
	private static final double MOUTH_HEIGHT = 0.03;

	
	public void run() {
		//double width = getWidth(); 
		//double height = getHeight();
		double width = 500;
		double height = 550;
		double x = getWidth() - width / 2;
		double y = getHeight() - height / 2;
		
		
		face = new GRect(x, y,width,height);
		face.setFilled(true);
		face.setFillColor(Color.GRAY);
		
		 leftEye = new GOval(EYE_WIDTH * width, EYE_HEIGHT * height);
	     rightEye = new GOval(EYE_WIDTH * width, EYE_HEIGHT * height);
	     
	     leftEye.setFilled(true);
	     leftEye.setFillColor(Color.YELLOW);
	     rightEye.setFilled(true);
	     rightEye.setFillColor(Color.YELLOW);
	     
	     mouth = new GRect(MOUTH_WIDTH * width, MOUTH_HEIGHT * height);
	     mouth.setFilled(true);
	     mouth.setFillColor(Color.WHITE);
	     //
	     //add(rightEye, 0.25 * width - EYE_WIDTH * width / 2,
         //         -0.25 * height - EYE_HEIGHT * height / 2);
	     //leftEye.setLocation(-0.25 * x - EYE_WIDTH ,
	    	//	 -0.25 * y - EYE_HEIGHT);
	     add(face);
	     add(mouth, 0.25 * x + x,2 * y + y);
	    
	     add(leftEye, 0.20 * x + x , 0.25 * y + y );
	     add(rightEye, 0.70 * x + x , 0.25 * y + y );
		
		

	}
	
	private GOval leftEye, rightEye;
	private GRect mouth, face;
	
}
	
